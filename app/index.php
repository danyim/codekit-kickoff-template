<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8">
    <title>Daniel D. Yim</title>
    <meta name="title" content="Daniel D. Yim">
    <meta name="keywords" content="Daniel Yim, danyim, nubs.org, Daniel D Yim, daniel yim houston">
    <meta name="description" content="Daniel D. Yim's personal website containing projects, professional information, and a small summary.">
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Droid+Sans">
    <link rel="stylesheet" type="text/css" href="/_/css/app.css">
</head>
<body>
    <div id="wrapper">
        <div id="top">
            <div class="whoIs">who is</div>
            <div class="name">
                <span class="carat">&gt;&gt;</span><a href="#">daniel yim</a><span>?</span>
            </div>
            <div class="nav">
                [ <a href="#about">about</a> &middot; <a href="#resume">resume</a> &middot; <a href="#projects">projects</a> ]
            </div>
        </div>
        <div id="contentContainer">
            <!-- MAIN -->
            <h1>welcome</h1>
            <p>Welcome to my personal website.</p>
            <p>I intend this space to be a hub for all things related to me, the Daniel Yim of Houston, Texas, as an attempt to disambiguate myself from the others who share the name. Although I know that "Daniel Yim" is relatively common on the Internet and probably the whole world, I'd like to think that I am the only computer scientist/web developer among them. I'm probably dead wrong.</p>
            <p>This website--a subdomain of <a href="http://nubs.org">nubs.org</a> (an old asset of mine)--is a sandbox where I can test/play with new web technologies (HTML5, CSS3) for my own entertainment and, to be completely honest, for showing off.</p>
            <p>I may soon change the colorless theme to something more...lively, but until then please deal with the monochrome.</p>

            <!-- ABOUT -->
            <h1><a name="about">about me</a></h1>
            <p>I graduated in 2011 from Stephen F. Austin State University with a B.S. in Computer Science. As an undergrad, I kept my academic focus in CS, but I also minored in mathematics to challenge my (then) mathematical immaturity. Eventually, I grew equally fond of both disciplines and learned to appreciate their strong similarities.</p>
            <p>As far as computer science goes, my interests in that subject spreads across many different branches therein; however, I particularly enjoy learning about human-computer interaction, web technologies and design, computer security, and Android development.</p>
            <p>When I am not doing geeky stuff, you may find me playing the blues on my cherished 1962 Reissue Japanese Fender Stratocaster or out on a neverending quest to find good eats around town.</p>
            <p>You can contact me at <span style="text-decoration: underline;">(MY FULL NAME)_at_gmail.com</span>.</p>

            <!-- RESUME -->
            <h1><a name="resume">resume & professional info</a></h1>
            <p>At the moment, I am open to any opportunities where I can capitalize on my programming, designing, and teamwork abilities.</p>
            <p>My professional information can be found through <a href="http://www.linkedin.com/in/danielyim" style="text-decoration: underline;">my LinkedIn account</a>.</p>
            <p>Please feel free to send me an email to request a resume.</p>

            <!-- PROJECTS -->
            <h1><a name="projects">projects</a></h1>
            <p>I try to expand my horizons as a developer, so beside the projects that I work on at my nine-to-five, I maintain some side development projects to occupy my time.</p>
            <h2>mobile development</h2>
            <h2>web design</h2>
            <ul>
                <li><a href="http://www.gulfcoastexoticbirdsanctuary.com/">Gulf Coast Exotic Bird Sanctuary</a><br>
                I frequently volunteer my time at this wonderful non-profit organization that provides a safe environment for birds and other exotic animals.</li>
            </ul>
            <h2>miscellaneous</h2>
            <ul>
                <li><a href="http://stackoverflow.com/users/350951/danyim">Stack Overflow</a><br>
                My SO account isn't necessarily packed with insightful questions or exceptional answers, but I aim to contribute to the community when I can.</li>
            </ul>
            <ul>
                <li><a href="http://projecteuler.net/profile/danyim.png">Project Euler</a><br>
                My goal is to work though most (if not all) of these mathematically inclined programming problems strictly in Python in order to help me learn the language. I will post my solutions soon.</li>
            </ul>
            <ul>
                <li><a href="http://topcoder.com/">TopCoder</a></li>
            </ul>
            <h2>open source</h2>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <div id="infoBox">
            page last updated: <span class="date">4 Jul 2011</span>
        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
    <script type="text/javascript" src="/_/js/app.js"></script></script>
</body>
</html>